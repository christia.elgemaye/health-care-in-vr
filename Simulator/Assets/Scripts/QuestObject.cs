﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestObject : MonoBehaviour
{
    public Data mydata;

    [Serializable] // serialize the Data struct
    public struct Data
    {
        public String questTitle;
        public String questDesc;
        public String questType;
        public float x;
        public float y;
        public float z;
    }

    public QuestObject(String questTitleArg, String questDescArg, String questTypeArg, float xArg, float yArg, float zArg)
    {
        mydata.questTitle = questTitleArg;
        mydata.questDesc = questDescArg;
        mydata.questType = questTypeArg;
        mydata.x = xArg;
        mydata.y = yArg;
        mydata.z = zArg;
    }
}
