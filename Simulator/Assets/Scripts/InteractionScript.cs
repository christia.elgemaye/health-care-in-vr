﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionScript : MonoBehaviour
{
    public string triggerTag;
    public string triggeredText;

    private Canvas canvas;
    private Text textComponent;


    private void Start()
    {
        canvas = (Canvas)FindObjectOfType(typeof(Canvas));
        textComponent = canvas.GetComponentInChildren<Text>();

    }





    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == triggerTag)
        {
            textComponent.text = triggeredText;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.tag == triggerTag)
        {
            textComponent.text = "";
        }
    }
}
