﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.AI;

public class PatrolArea : MonoBehaviour
{

    public ManagerScript managerScript;
    public Material patrolAreaMaterial;
    [HideInInspector]
    public Dictionary<GameObject, GameObject> patrolAreas = new Dictionary<GameObject, GameObject>();

    private float x;
    private float z;
    private bool willMove;
    private NavMeshAgent navMeshAgent;
    private GameObject patrolArea;
    private Animator animator;

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("patrol");
    }



    void patrol()
    {
        if (patrolAreas.Count == 0) return;
        foreach (GameObject item in patrolAreas.Keys)
        {

            navMeshAgent = item.GetComponent<NavMeshAgent>();
            if (item.gameObject.TryGetComponent<Animator>(out animator))
            {
                if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
                {
                    animator.SetBool("isWalking", true);
                }
                else
                {
                    animator.SetBool("isWalking", false);
                }
            }


            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude <= 5) navMeshAgent.acceleration = 500;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 8) navMeshAgent.acceleration = 8;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 5) continue;

            patrolAreas.TryGetValue(item, out patrolArea);

            x = Random.Range(patrolArea.transform.position.x - (patrolArea.transform.localScale.x / 2), patrolArea.transform.position.x + (patrolArea.transform.localScale.x / 2));
            z = Random.Range(patrolArea.transform.position.z - (patrolArea.transform.localScale.z / 2), patrolArea.transform.position.z + (patrolArea.transform.localScale.z / 2));
            willMove = Random.Range(0f, 1f) < 0.001f ? true : false;
            if (willMove == true)
            {
                navMeshAgent.isStopped = true;
                navMeshAgent.SetDestination(new Vector3(x, 1, z));
                navMeshAgent.isStopped = false;

            }
        }
    }

}
