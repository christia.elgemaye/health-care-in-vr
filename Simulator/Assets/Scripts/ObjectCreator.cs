﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCreator : MonoBehaviour
{
    public Dictionary<string, GameObject> itemDict = new Dictionary<string, GameObject>();
    public List<string> itemNames;
    public List<GameObject> itemPrefabs;

    [HideInInspector]
    public string itemOptionSelection = "Player";

    public ManagerScript managerScript;


    private void Awake()
    {
        for (int i = 0; (i < itemNames.Count) || (i < itemPrefabs.Count); i++)
        {
            itemDict.Add(itemNames[i], itemPrefabs[i]);
        }
    }



    public static void AddEditorData(GameObject newObj, string objType)
    {
        //Add editor object component and feed it data.
        EditorObject eo = newObj.AddComponent<EditorObject>();
        eo.data.pos = newObj.transform.position;
        eo.data.rot = newObj.transform.rotation;
        eo.data.scale = newObj.transform.localScale;
        eo.data.objectType = objType;
    }
}
