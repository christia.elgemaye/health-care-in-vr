﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.AI;
using UnityEngine.UI;

public class RouteScript : MonoBehaviour
{
    public ManagerScript managerScript;
    public GameObject xMark;
    public GameObject lineAsset;
    public InputField timeTrigger;

    [HideInInspector]
    public Dictionary<GameObject, List<Vector3>> routeDict = new Dictionary<GameObject, List<Vector3>>();
    [HideInInspector]
    public Dictionary<GameObject, float> routeTimerDict = new Dictionary<GameObject, float>();

    private RaycastHit hit;
    private List<Vector3> route = new List<Vector3>();
    private List<GameObject> waypoints = new List<GameObject>();
    private int i = 0;
    private Vector3 markerLocation;
    private GameObject marker;
    private Vector3 lineLocation;
    private Vector3 lineScale;
    private Quaternion lineRotation;
    private GameObject line;
    private bool placed = false;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    private List<Vector3> routeTaken = new List<Vector3>();
    private float targetTime;


    void Update()
    {
        StartCoroutine("routing");
    }


    void routing()
    {
        if (routeDict.Count == 0) return;
        foreach (GameObject item in routeDict.Keys)
        {

            navMeshAgent = item.GetComponent<NavMeshAgent>();
            if (item.gameObject.TryGetComponent<Animator>(out animator))
            {
                if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
                {
                    animator.SetBool("isWalking", true);
                }
                else
                {
                    animator.SetBool("isWalking", false);
                }
            }


            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude <= 5) navMeshAgent.acceleration = 500;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 8) navMeshAgent.acceleration = 8;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 5) continue;

            routeDict.TryGetValue(item, out routeTaken);
            routeTimerDict.TryGetValue(item, out targetTime);

            targetTime -= Time.deltaTime;

            if (targetTime <= 0.0f)
            {
                navMeshAgent.isStopped = true;
                navMeshAgent.SetDestination(routeTaken[0]);
                navMeshAgent.isStopped = false;

                if ((navMeshAgent.destination - item.transform.position).sqrMagnitude <= 0.01)
                {
                    if (routeTaken.Count > 1)
                    {
                        routeTaken.RemoveAt(0);
                        routeDict.Remove(item);
                        routeDict.Add(item, routeTaken);
                        break;
                    }
                }
            }
            else
            {
                routeTimerDict.Remove(item);
                routeTimerDict.Add(item, targetTime);
            }
        }
    }
}
