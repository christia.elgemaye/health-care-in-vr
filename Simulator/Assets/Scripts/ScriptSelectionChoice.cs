﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ScriptSelectionChoice : MonoBehaviour
{
    public ManagerScript managerScript;
    public List<string> scriptNames;

    private Throwable T;
    private Interactable I;
    private BoxCollider boxCollider;

    
    public void addScript(string option, ref Transform assetTransform)
    {
        Debug.Log(assetTransform);
        if (assetTransform.GetComponent<EditorObject>().data.scripts == null)
        {
            assetTransform.GetComponent<EditorObject>().data.scripts = new List<string>();
        }

        if (option.Equals("Rigid Body"))
        {
            assetTransform.gameObject.AddComponent<Rigidbody>();
        }

        if (option.Equals("Throwable"))
        {
            
            if(!assetTransform.gameObject.TryGetComponent<Throwable>(out T))
            {
                assetTransform.gameObject.AddComponent<Throwable>();
            }
            
            
        }

        if (option.Equals("Interactable"))
        {
            
            if (!assetTransform.gameObject.TryGetComponent<Interactable>(out I))
            {
                assetTransform.gameObject.AddComponent<Interactable>();
            }
            
        }

    }

    public void addInteractionScript(string triggerOption, string textOption, ref Transform assetTransform)
    {
        assetTransform.GetComponent<EditorObject>().data.trigger = triggerOption ;
        assetTransform.GetComponent<EditorObject>().data.textTriggered = textOption ;

        boxCollider = assetTransform.gameObject.GetComponent<BoxCollider>();
        BoxCollider NewboxCollider = assetTransform.gameObject.AddComponent<BoxCollider>(boxCollider);
        Debug.Log(NewboxCollider);
        NewboxCollider.isTrigger = true;
        assetTransform.gameObject.AddComponent<InteractionScript>();
        InteractionScript interactionScript = assetTransform.gameObject.GetComponent<InteractionScript>();
        interactionScript.triggerTag = triggerOption;
        interactionScript.triggeredText = textOption;
         
    }


}
