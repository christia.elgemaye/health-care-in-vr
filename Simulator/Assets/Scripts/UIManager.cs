﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private bool journalInUse = false;
    public GameObject journal;
    public ManagerScript ms;

    public GameObject ViewQuest;
    public GameObject AddQuest;
    public GameObject questList;

    public InputField questInputTitle;
    public InputField questInputDesc;
    public Dropdown dropDown;

    public Text questTitle;
    public Text questDesc;
    public Text questType;

    public QuestingSystem questingSystem;

    private GameObject button;
    private bool selecting = false;
    private bool objectiveShow = false;
    private GameObject objectiveCube;
    private float x;
    private float y;
    private float z;


    public Material selectedObjective;



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            if (journalInUse)
            {
                journal.active = false;
                journalInUse = false;
                ms.saveLoadMenuOpen = false;
            }
            else
            {
                journal.active = true;
                journalInUse = true;
                ms.saveLoadMenuOpen = true;
            }
        }


        if (objectiveShow && (journal.active || Input.GetKeyDown(KeyCode.Escape)))
        {
            objectiveShow = false;
            Destroy(objectiveCube);
        }
    }

    public void AddButton()
    {
        ViewQuest.active = false;
        AddQuest.active = true;

        questTitle.text = "";
        questDesc.text = "";
        questType.text = "";
    }

    public void RemoveButton()
    { 
        Destroy(button);
        AddQuest.active = false;
        ViewQuest.active = false;

        questingSystem.questLog.RemoveAt(searchList(questingSystem.questLog,
            EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text));

        //        AddQuest.active = false;
        //        ViewQuest.active = false;
    }

    public void SaveButton()
    {
        questingSystem.questLog.Add(new QuestObject(questInputTitle.text, questInputDesc.text, dropDown.options[dropDown.value].text, x,y,z));
        questingSystem.UpdateList();

        questInputTitle.text = "";
        questInputDesc.text = "";
        questType.text = "";

    }

    public void DisplayQuest()
    {
        AddQuest.active = false;
        ViewQuest.active = true;

        QuestObject displayed = questingSystem.questLog[
            searchList(questingSystem.questLog,
                EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text)];

        questTitle.text = displayed.mydata.questTitle;
        questDesc.text = displayed.mydata.questDesc;
        questType.text = "Quest type: " + displayed.mydata.questType;
        button = EventSystem.current.currentSelectedGameObject;

//    Debug.Log(EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text);
    }

    public int searchList(List<QuestObject> lista, String title)
    {
        int i = 0;
        foreach (QuestObject etc in lista)
        {
            if (etc.mydata.questTitle == title) return i;
                i++;
        }
        return 0;
    }

    public void clearLog()
    {
        foreach (Button etc in questList.GetComponentsInChildren<Button>())
        {
            if (etc.tag == "Quest") Destroy(etc.gameObject);
        }
        questingSystem.questLog.Clear();
    }


    public void ShowObjective()
    {
        QuestObject.Data data = questingSystem.questLog[searchList(questingSystem.questLog,
            questTitle.text)].mydata;
        
        objectiveCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        objectiveCube.transform.position = new Vector3(data.x, data.y, data.z);
        objectiveCube.GetComponent<Renderer>().material = selectedObjective;

        journal.active = false;
        objectiveShow = true;
    }
}
