const express = require('express');
const MapRouter = express.Router();

const maps = require('./models/maps');

MapRouter.route('/create').post(function (req, res) {
	var json = req.body;

	maps.findOneAndDelete({name : req.headers["mapname"]}, (err, mapItem) => {
        if (err){
			res.status(400).send("unable to save to the database");
		}
		else {
			console.log("Overwriting");
		}
		})

	json["name"] = req.headers["mapname"];
	const mapItem = new maps(json);
	console.log(mapItem);
	mapItem.save()
	 .then(mapItem => {
		res.json('map Item added successfully');
	 })
	 .catch(err => {
		res.status(400).send("unable to save to the database");
	 });
});

MapRouter.route('/').post(function (req, res) {
	const myid = new maps(req.body)
	maps.findOne({name : myid.name}, (err, mapItem) => {
        console.log("specific map item fetched:", mapItem);
		res.send(mapItem)
		})
});

MapRouter.route('/getall').get(function (req, res) {
    maps.find({}, (err, mapItem) => {
        console.log("fetched all map items", mapItem);
        res.json(mapItem)
    })
});

MapRouter.route('/listall').get(function (req, res) {
    maps.find({}, (err, mapItem) => {
		var result = [];
		console.log(mapItem);
		for(let i=0; i < mapItem.length; i++){
			result.push(mapItem[i].name)
		}
		console.log("fetched all map items", result);
        res.json({ "maps" : result})
    })
});

module.exports = MapRouter;