﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class EditorObject : MonoBehaviour // inherit from monobehaviour to use as component in Unity.
{
    [Serializable] // serialize the Data struct
    public struct Data
    {
        public Vector3 pos; // the object's position
        public Quaternion rot; // the object's rotation
        public string objectType; // the type of object.
        public Vector3 scale;

        public List<string> scripts;

        public bool patrol;
        public Vector3 posPatrol;
        public Vector3 scalPatrol;

        public bool route;
        public List<Vector3> routeWaypoints;
        public float triggerTimer;

        public float range;
        public float spotAngle;
        public float intensity;

        public float R;
        public float G;
        public float B;
        public float A;
        public string item;

        public string tag;
        public string trigger;
        public string textTriggered;
    }

    public Data data; // public reference to Data
}
