﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSelectionChoice : MonoBehaviour
{
    public ManagerScript managerScript;
    public List<string> triggerNames;


    // Start is called before the first frame update
    void Start()
    {
        managerScript.TriggerSelectionDropdown.AddOptions(triggerNames);
        managerScript.TriggerSelectionDropdownInteraction.AddOptions(triggerNames);
    }

    public void addTrigger(string option, ref Transform assetTransform)
    {
        assetTransform.GetComponent<EditorObject>().data.tag = option;
    }

}
