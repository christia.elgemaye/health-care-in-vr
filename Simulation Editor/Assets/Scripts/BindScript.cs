﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BindScript : MonoBehaviour
{
    public ManagerScript managerScript;
    public RouteScript routeScript;
    public PatrolArea patrolArea;
    public void bind()
    {
        if(managerScript.mouseScript.manipulateOption == MouseScript.LevelManipulation.SelectRoute)
        {
            routeScript.bind();
        }
        else if(managerScript.mouseScript.manipulateOption == MouseScript.LevelManipulation.SelectPatrol)
        {
            patrolArea.bind();
        }
    }
}
