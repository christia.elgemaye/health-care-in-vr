﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptSelectionChoice : MonoBehaviour
{
    public ManagerScript managerScript;
    public List<string> scriptNames;

    void Start()
    {
        managerScript.ScriptSelectionDropdown.AddOptions(scriptNames);
    }
    
    public void addScript(string option, ref Transform assetTransform)
    {
        Debug.Log(assetTransform);
        if (assetTransform.GetComponent<EditorObject>().data.scripts == null)
        {
            assetTransform.GetComponent<EditorObject>().data.scripts = new List<string>();
        }

        if (option.Equals("Rigid Body"))
        {
            assetTransform.gameObject.AddComponent<Rigidbody>();
        }

        if (option.Equals("Throwable"))
        {
            /*
            if(!assetTransform.gameObject.TryGetComponent<Throwable>(out T))
            {
                assetTransform.gameObject.AddComponent<Throwable>();
            }
            */
        }

        if (option.Equals("Interactable"))
        {
            /*
            if (!assetTransform.gameObject.TryGetComponent<Interactable>(out I))
            {
                assetTransform.gameObject.AddComponent<Interactable>();
            }
            */
        }

        if(option.Equals("InteractionScript"))
        {
            managerScript.AddInteractionScript();
        }
        assetTransform.GetComponent<EditorObject>().data.scripts.Add(option);
    }

    public void addInteractionScript(string triggerOption, string textOption, ref Transform assetTransform)
    {
        assetTransform.GetComponent<EditorObject>().data.trigger = triggerOption ;
        assetTransform.GetComponent<EditorObject>().data.textTriggered = textOption ;

        /* assetTransform.gameObject.AddComponent<InteractionScript>();
         * InteractionScript interactionScript = assetTransform.gameObject.GetComponent<InteractionScript>();
         * interactionScript.triggerTag = triggerOption;
         * interactionScript.triggeredText = textOption;
         */
    }
}
