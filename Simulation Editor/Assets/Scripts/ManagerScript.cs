﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using System.Net.Http;
using System.Text;

public class ManagerScript : MonoBehaviour
{
    // Hide these variables from Unity editor.
    [HideInInspector]
    public bool playerPlaced = false;
    [HideInInspector]
    public bool saveLoadMenuOpen = false;

    public Animator itemUIAnimation;
    public Animator optionUIAnimation;
    public Animator NpcUIAnimation;
    public Animator EnvironmentUIAnimation;
    public Animator saveUIAnimation;
    public Animator loadUIAnimation;
    public MeshFilter mouseObject;
    public MouseScript mouseScript;
    public SelectionManager selectionManager;

    public Slider rotSlider;
    public GameObject rotUI;

    public InputField InputX;
    public InputField InputY;
    public InputField InputZ;
    public GameObject resizeUI;

    public InputField RepositionX;
    public InputField RepositionY;
    public InputField RepositionZ;
    public GameObject repositionUI;

    public InputField RescaleX;
    public InputField RescaleY;
    public InputField RescaleZ;
    public GameObject rescaleUI;
    public GameObject environmentParent;

    public InputField Range;
    public InputField SpotAngle;
    public InputField Intensity;
    public GameObject lightUI;

    public ScriptSelectionChoice ScriptSelectionChoice;
    public Dropdown ScriptSelectionDropdown;
    public GameObject scriptSelectionUI;
    private string option;

    public TriggerSelectionChoice TriggerSelectionChoice;
    public Dropdown TriggerSelectionDropdown;
    public GameObject triggerSelectionUI;
    private string triggerOption;

    public Dropdown TriggerSelectionDropdownInteraction;
    public InputField InteractionText;
    public GameObject InteractionScriptUI;
    private string textOption;
    private string triggerOptionInteraction;

    public InputField levelNameSave;
    public InputField levelNameLoad;
    public Text levelMessage;
    public Animator messageAnim;
    public ObjectCreator objectCreator;
    public Material patrolAreaMaterial;
    public PatrolArea patrolArea;
    public RouteScript routeScript;
    public GameObject routeTimeUI;

    public GameObject colorChangeUI;
    public Material wallMat;
    public Material floorMat;
    public Material ceilingMat;
    public Dropdown materialChoice;
    public InputField colorR;
    public InputField colorG;
    public InputField colorB;
    public InputField colorA;
    public GameObject wallMatSaver;
    public GameObject floorMatSaver;
    public GameObject ceilingMatSaver;
    public GameObject ceiling;

    public QuestingSystem questingSystem;
    public UIManager uiManager;



    private bool itemPositionIn = true;
    private bool optionPositionIn = true;
    private bool NpcPositionIn = true;
    private bool EnvironmentPositionIn = true;
    private bool saveLoadPositionIn = false;
    private LevelEditor level;
    private bool ChangingUI = false;
    private bool ceilingStatus = false;


    // Start is called before the first frame update
    void Start()
    {
        createEnvironmentObject();
        rotSlider.onValueChanged.AddListener(delegate { RotationValueChange(); }); // set up listener for rotation slider value change
        InputX.onValueChanged.AddListener(delegate { ResizeValueChange(); }); // set up listener for resize x slider value change
        InputY.onValueChanged.AddListener(delegate { ResizeValueChange(); }); // set up listener for resize y slider value change
        InputZ.onValueChanged.AddListener(delegate { ResizeValueChange(); }); // set up listener for resize z slider value change
        RescaleX.onValueChanged.AddListener(delegate { RescaleValueChange(); }); // set up listener for rescale x slider value change
        RescaleY.onValueChanged.AddListener(delegate { RescaleValueChange(); }); // set up listener for rescale y slider value change
        RescaleZ.onValueChanged.AddListener(delegate { RescaleValueChange(); }); // set up listener for rescale z slider value change
        RepositionX.onValueChanged.AddListener(delegate { RepositionValueChange(); }); // set up listener for reposition x slider value change
        RepositionY.onValueChanged.AddListener(delegate { RepositionValueChange(); }); // set up listener for reposition y slider value change
        RepositionZ.onValueChanged.AddListener(delegate { RepositionValueChange(); }); // set up listener for reposition z slider value change
        Range.onValueChanged.AddListener(delegate { LightValueChange(); }); // set up listener for Range slider value change
        SpotAngle.onValueChanged.AddListener(delegate { LightValueChange(); }); // set up listener for SpotAngle slider value change
        Intensity.onValueChanged.AddListener(delegate { LightValueChange(); }); // set up listener for Intensity slider value change
        colorR.onValueChanged.AddListener(delegate { environmentColorChange(); }); // set up listener for R slider value change
        colorG.onValueChanged.AddListener(delegate { environmentColorChange(); }); // set up listener for G slider value change
        colorB.onValueChanged.AddListener(delegate { environmentColorChange(); }); // set up listener for B slider value change
        colorA.onValueChanged.AddListener(delegate { environmentColorChange(); }); // set up listener for B slider value change
        CreateEditor(); // create new instance of level.
    }

    void createEnvironmentObject()
    {
        EditorObject eo = environmentParent.AddComponent<EditorObject>();
        eo.data.pos = environmentParent.transform.position;
        eo.data.rot = environmentParent.transform.rotation;
        eo.data.scale = environmentParent.transform.localScale;
        eo.data.objectType = "environment";
    }

    LevelEditor CreateEditor()
    {
        level = new LevelEditor();
        level.editorObjects = new List<EditorObject.Data>(); // make new list of editor object data.
        return level;
    }

    //Rotating an object and saving the info
    void RotationValueChange()
    {
        for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
        {
            selectionManager.selectedUnits[i].transform.localEulerAngles = new Vector3(selectionManager.selectedUnits[i].transform.localEulerAngles.x, rotSlider.value, selectionManager.selectedUnits[i].transform.localEulerAngles.z); // rotate the object.
            selectionManager.selectedUnits[i].GetComponent<EditorObject>().data.rot = selectionManager.selectedUnits[i].transform.rotation; // save sd info to object's editor object data. 
        }
    }

    void ResizeValueChange()
    {
        if (ChangingUI == false)
        {
            for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
            {
                if (InputX.text.Equals("") == false)
                {
                    selectionManager.selectedUnits[i].transform.localScale = new Vector3(float.Parse(InputX.text), selectionManager.selectedUnits[i].transform.localScale.y, selectionManager.selectedUnits[i].transform.localScale.z); // resize the object.
                }
                if (InputY.text.Equals("") == false)
                {
                    selectionManager.selectedUnits[i].transform.localScale = new Vector3(selectionManager.selectedUnits[i].transform.localScale.x, float.Parse(InputY.text), selectionManager.selectedUnits[i].transform.localScale.z); // resize the object.
                }
                if (InputZ.text.Equals("") == false)
                {
                    selectionManager.selectedUnits[i].transform.localScale = new Vector3(selectionManager.selectedUnits[i].transform.localScale.x, selectionManager.selectedUnits[i].transform.localScale.y, float.Parse(InputZ.text)); // resize the object.
                }
                if (InputX.text == "" && InputY.text == "" && InputZ.text == "")
                {
                    break;
                }
                selectionManager.selectedUnits[i].GetComponent<EditorObject>().data.scale = selectionManager.selectedUnits[i].transform.localScale; // save rotation info to object's editor object data.
            }
        }
    }

    void RescaleValueChange()
    {
        if (ChangingUI == false)
        {
            if (RescaleX.text.Equals("") == false)
            {
                environmentParent.transform.localScale = new Vector3(float.Parse(RescaleX.text), environmentParent.transform.localScale.y, environmentParent.transform.localScale.z); // resize the object.
            }
            if (RescaleY.text.Equals("") == false)
            {
                environmentParent.transform.localScale = new Vector3(environmentParent.transform.localScale.x, float.Parse(RescaleY.text), environmentParent.transform.localScale.z); // resize the object.
            }
            if (RescaleZ.text.Equals("") == false)
            {
                environmentParent.transform.localScale = new Vector3(environmentParent.transform.localScale.x, environmentParent.transform.localScale.y, float.Parse(RescaleZ.text)); // resize the object.
            }
            environmentParent.GetComponent<EditorObject>().data.pos = environmentParent.transform.localScale; // save rotation info to object's editor object data.     
        }
    }

    void RepositionValueChange()
    {
        if (ChangingUI == false)
        {
            for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
            {
                if (RepositionX.text.Equals("") == false)
                {
                    selectionManager.selectedUnits[i].transform.position = new Vector3(float.Parse(RepositionX.text), selectionManager.selectedUnits[i].transform.position.y, selectionManager.selectedUnits[i].transform.position.z); // resize the object.
                }
                if (RepositionY.text.Equals("") == false)
                {
                    selectionManager.selectedUnits[i].transform.position = new Vector3(selectionManager.selectedUnits[i].transform.position.x, float.Parse(RepositionY.text), selectionManager.selectedUnits[i].transform.position.z); // resize the object.
                }
                if (RepositionZ.text.Equals("") == false)
                {
                    selectionManager.selectedUnits[i].transform.position = new Vector3(selectionManager.selectedUnits[i].transform.position.x, selectionManager.selectedUnits[i].transform.position.y, float.Parse(RepositionZ.text)); // resize the object.
                }
                if (RepositionX.text == "" && RepositionY.text == "" && RepositionZ.text == "")
                {
                    break;
                }
                selectionManager.selectedUnits[i].GetComponent<EditorObject>().data.scale = selectionManager.selectedUnits[i].transform.position; // save rotation info to object's editor object data.
            }
        }
    }

    void LightValueChange()
    {
        Light lightComponent;
        for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
        {
            if (selectionManager.selectedUnits[i].TryGetComponent<Light>(out lightComponent))
            {
                if (Range.text.Equals("") == false)
                {
                    lightComponent.range = float.Parse(Range.text);
                }
                if (SpotAngle.text.Equals("") == false)
                {
                    lightComponent.spotAngle = float.Parse(SpotAngle.text);
                }
                if (Intensity.text.Equals("") == false)
                {
                    lightComponent.intensity = float.Parse(Intensity.text);
                }
                if (Range.text == "" && SpotAngle.text == "" && Intensity.text == "")
                {
                    break;
                }
                selectionManager.selectedUnits[i].GetComponent<EditorObject>().data.range = lightComponent.range;
                selectionManager.selectedUnits[i].GetComponent<EditorObject>().data.spotAngle = lightComponent.spotAngle;
                selectionManager.selectedUnits[i].GetComponent<EditorObject>().data.intensity = lightComponent.intensity;
            }
        }
    }

    void environmentColorChange()
    {
        wallMat.color = new Color(.8f, .8f, .8f, 1);
        string colorOption = materialChoice.options[materialChoice.value].text;
        Material chosenMat = wallMat;
        EditorObject eo = wallMatSaver.GetComponent<EditorObject>();

        if (colorOption.Equals("Walls"))
        {
            chosenMat = wallMat;
            eo = wallMatSaver.GetComponent<EditorObject>();
            eo.data.item = "Walls";
        }

        else if (colorOption.Equals("Floor"))
        {
            chosenMat = floorMat;
            eo = floorMatSaver.GetComponent<EditorObject>();
            eo.data.item = "Floor";
        }

        else if (colorOption.Equals("Ceiling"))
        {
            chosenMat = ceilingMat;
            eo = ceilingMatSaver.GetComponent<EditorObject>();
            eo.data.item = "Ceiling";
        }

        if (colorR.text.Equals("")== false && colorG.text.Equals("") == false && colorB.text.Equals("") == false && colorA.text.Equals("") == false)
        {
            chosenMat.color = new Color(float.Parse(colorR.text), float.Parse(colorG.text), float.Parse(colorB.text), float.Parse(colorA.text));
            eo.data.R = float.Parse(colorR.text);
            eo.data.G = float.Parse(colorG.text);
            eo.data.B = float.Parse(colorB.text);
            eo.data.A = float.Parse(colorA.text);
        }

    }

    public void ShowHideCeiling()
    {
        if (ceilingStatus)
        {
            ceilingStatus = false;
        }
        else
        {
            ceilingStatus = true;
        }

        ceiling.SetActive(ceilingStatus);
    }

    public void AddScriptButton()
    {
        for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
        {
            option = ScriptSelectionDropdown.options[ScriptSelectionDropdown.value].text;
            Transform selectedTransform = selectionManager.selectedUnits[i];
            ScriptSelectionChoice.addScript(option, ref selectedTransform);
        }
    }

    public void AddTriggerButton()
    {
        for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
        {
            triggerOption = TriggerSelectionDropdown.options[TriggerSelectionDropdown.value].text;
            Transform selectedTransform = selectionManager.selectedUnits[i];
            TriggerSelectionChoice.addTrigger(triggerOption, ref selectedTransform);

        }
    }

    public void AddInteractionScriptButton()
    {
        for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
        {
            triggerOptionInteraction = TriggerSelectionDropdownInteraction.options[TriggerSelectionDropdownInteraction.value].text;
            textOption = InteractionText.text;
            Transform selectedTransform = selectionManager.selectedUnits[i];
            ScriptSelectionChoice.addInteractionScript(triggerOptionInteraction, textOption, ref selectedTransform);
        }
    }


    public void SlideItemMenu()
    {
        SlideMenus(ref itemPositionIn, ref itemUIAnimation, "Item");
    }

    public void SlideOptionMenu()
    {
        SlideMenus(ref optionPositionIn, ref optionUIAnimation, "Option");
    }

    public void SlideNpcMenu()
    {
        SlideMenus(ref NpcPositionIn, ref NpcUIAnimation, "Npc");
    }

    public void SlideEnvironmentMenu()
    {
        SlideMenus(ref EnvironmentPositionIn, ref EnvironmentUIAnimation, "Environment");
    }

    private void SlideMenus(ref bool positionIn, ref Animator uiAnimation, string triggerText)
    { 
        if (positionIn == false)
        {
            uiAnimation.SetTrigger(triggerText + "MenuIn"); 
            positionIn = true; 
        }
        else
        {
            uiAnimation.SetTrigger(triggerText + "MenuOut"); 
            positionIn = false; 
        }
    }

    public void ChooseSave()
    {
        ChooseSaveOrLoad(saveUIAnimation);
    }

    public void ChooseLoad()
    {
        ChooseSaveOrLoad(loadUIAnimation);
    }

    private void ChooseSaveOrLoad(Animator loadOrSaveAnimator)
    {
        if (saveLoadPositionIn == false)
        {
            loadOrSaveAnimator.SetTrigger("SaveLoadIn"); // slide menu into screen
            saveLoadPositionIn = true; // indicate menu on screen
            saveLoadMenuOpen = true; // indicate load menu open, prevent camera movement.
        }
        else
        {
            loadOrSaveAnimator.SetTrigger("SaveLoadOut"); // slide menu off screen
            saveLoadPositionIn = false; // indicate menu off screen
            saveLoadMenuOpen = false; // indicate load menu off screen, allow camera movement.
        }
    }

    public void ChooseCreatedItem(string createdAsset)
    {
        UIManager(MouseScript.LevelManipulation.Create, true, false, false, false, false, false, false, false, false, false, false);
        objectCreator.itemOptionSelection = createdAsset;
        mouseObject.mesh = objectCreator.meshDict[createdAsset];
        mouseObject.transform.rotation = objectCreator.itemDict[createdAsset].transform.rotation;
    }

    public void ChooseRotate()
    {
        UIManager(MouseScript.LevelManipulation.Rotate, false, true, false, false, false, false, false, false, false, false, false);
    }

    public void ChooseResize()
    {
        UIManager(MouseScript.LevelManipulation.Resize, false, false, true, false, false, false, false, false, false, false, false);
        SetResizeObject();
    }

    public void ChooseRescaleEnvironment()
    {
        UIManager(MouseScript.LevelManipulation.Rescale, false, false, false, true, false, false, false, false, false, false, false);
        SetRescaleObject();
    }

    public void ChooseReposition()
    {
        UIManager(MouseScript.LevelManipulation.Reposition, false, false, false, false, true, false, false, false, false, false, false);
        SetRpositionObject();
    }

    public void ChooseSelect()
    {
        UIManager(MouseScript.LevelManipulation.Select, false, false, false, false, false, false, false, false, false, false, false);
    }

    public void PatrolAreaSelect()
    {
        UIManager(MouseScript.LevelManipulation.SelectPatrol, false, false, false, false, false, false, false, false, false, false, false);
    }

    public void TeleportAreaSelect()
    {
        UIManager(MouseScript.LevelManipulation.AddTeleport, false, false, false, false, false, false, false, false, false, false, false);
    }

    public void RouteSelect()
    {
        UIManager(MouseScript.LevelManipulation.SelectRoute, false, false, false, false, false, false, true, false, false, false, false);
    }

    public void AddScript()
    {
        UIManager(MouseScript.LevelManipulation.AddScript, false, false, false, false, false, true, false, false, false, false, false);
    }

    public void ChangeLight()
    {
        UIManager(MouseScript.LevelManipulation.ChangeLight, false, false, false, false, false, false, false, true, false, false, false);
    }

    public void ChangeColor()
    {
        UIManager(MouseScript.LevelManipulation.ChangeColor, false, false, false, false, false, false, false, false, true, false, false);
    }

    public void AddTrigger()
    {
        UIManager(MouseScript.LevelManipulation.AddTrigger, false, false, false, false, false, true, false, false, false, true, false);
    }

    public void AddInteractionScript()
    {
        UIManager(MouseScript.LevelManipulation.AddTrigger, false, false, false, false, false, false, false, false, false, false, true);
    }

    private void UIManager(MouseScript.LevelManipulation setMode, bool mouseMeshStatus, bool rotUIStatus, bool resizeUIStatus, bool rescaleUIStatus, bool repositionUIStatus, bool scriptSelectionUIStatus, bool routeTimeUIStatus, bool lightUIStatus, bool colorUIStatus, bool triggerUIStatus, bool InteractionUIStatus)
    {
        mouseScript.manipulateOption = setMode; // set mode
        mouseScript.mr.enabled = mouseMeshStatus; // hide mouse mesh
        rotUI.SetActive(rotUIStatus);
        resizeUI.SetActive(resizeUIStatus);
        rescaleUI.SetActive(rescaleUIStatus);
        repositionUI.SetActive(repositionUIStatus);
        scriptSelectionUI.SetActive(scriptSelectionUIStatus);
        routeTimeUI.SetActive(routeTimeUIStatus);
        lightUI.SetActive(lightUIStatus);
        colorChangeUI.SetActive(colorUIStatus);
        triggerSelectionUI.SetActive(triggerUIStatus);
        InteractionScriptUI.SetActive(InteractionUIStatus);
    }

    private void SetResizeObject()
    {
        ChangingUI = true;
        string InputXPlaceholder = "";
        string InputYPlaceholder = "";
        string InputZPlaceholder = "";
        for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
        {
            if (i == 0)
            {
                InputXPlaceholder = selectionManager.selectedUnits[i].transform.localScale.x.ToString();
                InputYPlaceholder = selectionManager.selectedUnits[i].transform.localScale.y.ToString();
                InputZPlaceholder = selectionManager.selectedUnits[i].transform.localScale.z.ToString();
            }
            else
            {
                Debug.Log("test");
                if (selectionManager.selectedUnits[i].transform.localScale.x.ToString() != InputXPlaceholder)
                {
                    InputXPlaceholder = "";
                }
                if (selectionManager.selectedUnits[i].transform.localScale.y.ToString() != InputYPlaceholder)
                {
                    InputYPlaceholder = "";
                }
                if (selectionManager.selectedUnits[i].transform.localScale.z.ToString() != InputZPlaceholder)
                {
                    InputZPlaceholder = "";
                }
            }
        }
        InputX.text = InputXPlaceholder; // set slider to current object's size.
        InputY.text = InputYPlaceholder; // set slider to current object's size.
        InputZ.text = InputZPlaceholder; // set slider to current object's size.
        ChangingUI = false;
    }

    private void SetRescaleObject()
    {
        RescaleX.text = environmentParent.transform.localScale.x.ToString(); // set slider to current object's size.
        RescaleY.text = environmentParent.transform.localScale.y.ToString(); // set slider to current object's size.
        RescaleZ.text = environmentParent.transform.localScale.z.ToString(); // set slider to current object's size.
    }

    private void SetRpositionObject()
    {
        ChangingUI = true;
        string RepositionXPlaceholder = "";
        string RepositionYPlaceholder = "";
        string RepositionZPlaceholder = "";
        for (int i = 0; i < selectionManager.selectedUnits.Count; i++)
        {
            if (i == 0)
            {
                RepositionXPlaceholder = selectionManager.selectedUnits[i].transform.position.x.ToString();
                RepositionYPlaceholder = selectionManager.selectedUnits[i].transform.position.y.ToString();
                RepositionZPlaceholder = selectionManager.selectedUnits[i].transform.position.z.ToString();
            }
            else
            {
                Debug.Log("test");
                if (selectionManager.selectedUnits[i].transform.position.x.ToString() != RepositionXPlaceholder)
                {
                    RepositionXPlaceholder = "";
                }
                if (selectionManager.selectedUnits[i].transform.position.y.ToString() != RepositionYPlaceholder)
                {
                    RepositionYPlaceholder = "";
                }
                if (selectionManager.selectedUnits[i].transform.position.z.ToString() != RepositionZPlaceholder)
                {
                    RepositionZPlaceholder = "";
                }
            }
        }
        RepositionX.text = RepositionXPlaceholder; // set slider to current object's size.
        RepositionY.text = RepositionYPlaceholder; // set slider to current object's size.
        RepositionZ.text = RepositionZPlaceholder; // set slider to current object's size.
        ChangingUI = false;
    }

    // Saving a level
    public async void SaveLevel()
        {
            level.editorObjects = new List<EditorObject.Data>();
            level.questObjects = new List<QuestObject.Data>();

            // Gather all objects with EditorObject component
            EditorObject[] foundObjects = FindObjectsOfType<EditorObject>();
            foreach (EditorObject obj in foundObjects)
                level.editorObjects.Add(obj.data); // add these objects to the list of editor objects

            foreach (QuestObject obj in questingSystem.questLog)
                level.questObjects.Add(obj.mydata);

        string json = JsonUtility.ToJson(level); // write the level data to json
            string folder = Application.dataPath + "/LevelData/"; // create a folder
            string levelFile;

            //set a default file name if no name given
            if (levelNameSave.text == "")
                levelFile = "new_level.json";
            else
                levelFile = levelNameSave.text + ".json";

            //Create new directory if LevelData directory does not yet exist.
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            string path = Path.Combine(folder, levelFile); // set filepath

        //Overwrite file with same name, if applicable
            if (File.Exists(path))
                File.Delete(path);
                

            // create and save file
            File.WriteAllText(path, json);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("mapName", levelNameSave.text);
                var response = await client.PostAsync(
                    "http://127.0.0.1:6003/map/create",
                    new StringContent(json, Encoding.UTF8, "application/json"));
                json = await response.Content.ReadAsStringAsync();
                Debug.Log(json);
            }

        //Remove save menu
        saveUIAnimation.SetTrigger("SaveLoadOut");
            saveLoadPositionIn = false;
            saveLoadMenuOpen = false;
            levelNameSave.text = ""; // clear input field
            levelNameSave.DeactivateInputField(); // remove focus from input field.

            //Display message
            levelMessage.text = levelFile + " saved to LevelData folder.";
            messageAnim.Play("MessageFade", 0, 0);
        }



    // Loading a level
    public async void LoadLevel()
    {
        string folder = Application.dataPath + "/LevelData/";
        string levelFile;
        string myJson = "";

        //set a default file name if no name given
        if (levelNameLoad.text == "")
        {
            levelFile = "new_level.json";
        }
        else
        {

            myJson = "{\"name\": \"" + levelNameLoad.text + "\"}";
            using (var client = new HttpClient())
            {
                var response = await client.PostAsync(
                    "http://127.0.0.1:6003/map/",
                    new StringContent(myJson, Encoding.UTF8, "application/json"));
                myJson = await response.Content.ReadAsStringAsync();
            }

            levelFile = levelNameLoad.text + ".json";
        }

        string path = Path.Combine(folder, levelFile); // set filepath

        if (myJson.Length > 2) // if the file could be found in LevelData
        {
            // The objects currently in the level will be deleted
            EditorObject[] foundObjects = FindObjectsOfType<EditorObject>();
            foreach (EditorObject obj in foundObjects)
                if (obj.gameObject.GetComponent<EditorObject>().data.objectType.Equals("environment") == false)
                {
                    Destroy(obj.gameObject);
                    if (patrolArea.patrolAreas.ContainsKey(obj.gameObject))
                    {
                        patrolArea.patrolAreas.Remove(obj.gameObject);
                    }
                    if (routeScript.routeDict.ContainsKey(obj.gameObject))
                    {
                        routeScript.routeDict.Remove(obj.gameObject);
                        routeScript.routeTimerDict.Remove(obj.gameObject);
                    }
                }
            playerPlaced = false;
            level = JsonUtility.FromJson<LevelEditor>(myJson);
            CreateFromFile(); 
        }
        else 
        {
            loadUIAnimation.SetTrigger("SaveLoadOut"); // remove menu
            saveLoadPositionIn = false; // indicate menu not on screen
            saveLoadMenuOpen = false; // indicate camera can move.
            levelMessage.text = levelFile + " could not be found!"; // send message
            messageAnim.Play("MessageFade", 0, 0);
            levelNameLoad.DeactivateInputField(); // remove focus from input field
        }
    }

    // create objects based on data within level.
    void CreateFromFile()
        {
        GameObject newObj; // make a new object.
        patrolArea.patrolAreas.Clear();
        for (int i = 0; i < level.editorObjects.Count; i++)
        {
            foreach (KeyValuePair<string, GameObject> entry in objectCreator.itemDict)
            {
                if (level.editorObjects[i].objectType.Equals(entry.Key)) // find what the object type is
                {
                    newObj = Instantiate(entry.Value);
                    newObj.transform.position = level.editorObjects[i].pos; // set position from data in level
                    newObj.transform.rotation = level.editorObjects[i].rot; // set rotation from data in level.
                    newObj.transform.localScale = level.editorObjects[i].scale; // set rotation from data in level.
                    newObj.tag = "UserCreated";
                    newObj.layer = 9; // set to Spawned Objects layer
                    if (level.editorObjects[i].patrol == true && level.editorObjects[i].posPatrol != null && level.editorObjects[i].scalPatrol != null)
                    {
                        createPatrol(newObj, level.editorObjects[i].posPatrol, level.editorObjects[i].scalPatrol);
                    }

                    if (level.editorObjects[i].route == true && level.editorObjects[i].routeWaypoints != null && level.editorObjects[i].triggerTimer != Mathf.Infinity)
                    {
                        createRoute(newObj, level.editorObjects[i].routeWaypoints, level.editorObjects[i].triggerTimer);
                    }
                    //Add editor object component and feed data.
                    ObjectCreator.AddEditorData(newObj, entry.Key);
                    foreach (string script in level.editorObjects[i].scripts)
                    {
                        Transform newObjTransform = newObj.transform;
                        if (script.Equals("InteractionScript"))
                        {
                            ScriptSelectionChoice.addInteractionScript(level.editorObjects[i].trigger, level.editorObjects[i].textTriggered, ref newObjTransform);
                        }
                        else
                        {
                            ScriptSelectionChoice.addScript(script, ref newObjTransform);
                        }  
                    }
                    Light lightComponent;
                    if (newObj.TryGetComponent<Light>(out lightComponent))
                    {
                        if (!float.IsNaN(level.editorObjects[i].range))
                        {
                            lightComponent.range = level.editorObjects[i].range;
                        }
                        if (!float.IsNaN(level.editorObjects[i].spotAngle))
                        {
                            lightComponent.spotAngle = level.editorObjects[i].spotAngle;
                        }
                        if (!float.IsNaN(level.editorObjects[i].intensity))
                        {
                            lightComponent.intensity = level.editorObjects[i].intensity;
                        }
                    }
                    /*
                    if(level.editorObjects[i].tag != null)
                    {
                        newObj.tag = level.editorObjects[i].tag;
                    }
                    */
                }
                else if (level.editorObjects[i].objectType.Equals("environment"))
                {
                    environmentParent.transform.position = level.editorObjects[i].pos; // set position from data in level
                    environmentParent.transform.rotation = level.editorObjects[i].rot; // set rotation from data in level.
                    environmentParent.transform.localScale = level.editorObjects[i].scale; // set rotation from data in level.
                }
            }
            if (level.editorObjects[i].objectType.Equals("colors"))
            {
                string colorSelectionLoad = level.editorObjects[i].item;
                Material chosenMat = wallMat;

                if (colorSelectionLoad.Equals("Walls"))
                {
                    chosenMat = wallMat;
                }

                else if (colorSelectionLoad.Equals("Floor"))
                {
                    chosenMat = floorMat;
                }

                else if (colorSelectionLoad.Equals("Ceiling"))
                {
                    chosenMat = ceilingMat;
                }
                else
                {
                    continue;
                }

                chosenMat.color = new Color(level.editorObjects[i].R, level.editorObjects[i].G, level.editorObjects[i].B, level.editorObjects[i].A);

            }
        }

            createQuests(level.questObjects);
            //Clear level box
            levelNameLoad.text = "";
            levelNameLoad.DeactivateInputField(); // remove focus from input field

            loadUIAnimation.SetTrigger("SaveLoadOut"); // slide load menu off screen
            saveLoadPositionIn = false; // indicate load menu off screen
            saveLoadMenuOpen = false; // allow camera movement.

            //Display message
            levelMessage.text = "Level loading...done.";
            messageAnim.Play("MessageFade", 0, 0);
        }

    private void createPatrol(GameObject patroller,Vector3 pos, Vector3 scale)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = pos;
        cube.transform.localScale = scale;
        cube.gameObject.GetComponent<Renderer>().material = patrolAreaMaterial;
        MeshRenderer meshRenderer = cube.gameObject.GetComponent<MeshRenderer>();
        meshRenderer.receiveShadows = false;
        meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        NavMeshAgent navMeshAgent;

        if (!patroller.TryGetComponent<NavMeshAgent>(out navMeshAgent))
        {
            navMeshAgent = patroller.AddComponent<NavMeshAgent>();
            navMeshAgent.speed = 1.5f;
            navMeshAgent.acceleration = 8;
            navMeshAgent.angularSpeed = 10000;
        }
        patrolArea.patrolAreas.Add(patroller, cube);
    }

    private void createRoute(GameObject objToRoute, List<Vector3> route, float triggerTimer)
    {
        NavMeshAgent navMeshAgent;

        if (!objToRoute.TryGetComponent<NavMeshAgent>(out navMeshAgent))
        {
            navMeshAgent = objToRoute.AddComponent<NavMeshAgent>();
            navMeshAgent.speed = 1.5f;
            navMeshAgent.acceleration = 8;
            navMeshAgent.angularSpeed = 10000;
        }
        routeScript.routeDict.Add(objToRoute, route);
        routeScript.routeTimerDict.Add(objToRoute, triggerTimer);
    }


    private void createQuests(List<QuestObject.Data> levelQuests)
    {
        uiManager.clearLog();
        for (int etc = 0; etc < levelQuests.Count; etc++)
        {
            questingSystem.questLog.Add(new QuestObject(levelQuests[etc].questTitle, levelQuests[etc].questDesc, levelQuests[etc].questType, levelQuests[etc].x, levelQuests[etc].y, levelQuests[etc].z));
        }
        questingSystem.initList();
    }


}