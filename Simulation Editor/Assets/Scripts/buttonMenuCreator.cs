﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonMenuCreator : MonoBehaviour
{

    public ObjectCreator oc;
    public GameObject buttonPrefab;
    private ManagerScript ms;
    private ButtonData buttonData;
    private RectTransform buttonTransform;
    private string buttonName;
    private int buttonPos;

    private void Update()
    {
        buttonPos = -50;
        ms = oc.managerScript;
        foreach (KeyValuePair<string, Sprite> entry in oc.iconDict)
        {
            if (transform.childCount < oc.iconDict.Count)
            {
                var newButton = Instantiate(buttonPrefab);
                newButton.transform.SetParent(gameObject.transform);
                buttonName = entry.Key + "Button";
                newButton.name = buttonName;

                
                buttonTransform = newButton.GetComponent<RectTransform>();
                buttonTransform.anchoredPosition = new Vector3(0, buttonPos, 0);
                buttonPos -= 80;

                buttonData = newButton.GetComponent<ButtonData>();
                buttonData.createdAsset = entry.Key;
                buttonData.buttonIcon = entry.Value;
                buttonData.ms = ms;
            }
        }
    }
}
