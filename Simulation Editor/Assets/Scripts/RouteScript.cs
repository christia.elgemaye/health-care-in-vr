﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.AI;
using UnityEngine.UI;

public class RouteScript : MonoBehaviour
{
    public ManagerScript managerScript;
    public GameObject xMark;
    public GameObject lineAsset;
    public InputField timeTrigger;

    [HideInInspector]
    public Dictionary<GameObject, List<Vector3>> routeDict = new Dictionary<GameObject, List<Vector3>>();
    [HideInInspector]
    public Dictionary<GameObject, float> routeTimerDict = new Dictionary<GameObject, float>();

    private RaycastHit hit;
    private List<Vector3> route = new List<Vector3>();
    private List<GameObject> waypoints = new List<GameObject>();
    private int i = 0;
    private Vector3 markerLocation;
    private GameObject marker;
    private Vector3 lineLocation;
    private Vector3 lineScale;
    private Quaternion lineRotation;
    private GameObject line;
    private bool placed = false;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    private List<Vector3> routeTaken = new List<Vector3>();
    private float targetTime;
    

    void Update()
    {
        StartCoroutine("routing");
        if (managerScript.mouseScript.manipulateOption == MouseScript.LevelManipulation.SelectRoute && !EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                var camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(camRay, out hit))
                {
                    markerLocation = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                    route.Add(markerLocation);
                    marker = Instantiate(xMark);
                    marker.transform.position = markerLocation;
                    waypoints.Add(marker);
                    if (i != 0)
                    {
                        line = Instantiate(lineAsset);

                        lineLocation = new Vector3((hit.point.x + route[i - 1].x) / 2, (hit.point.y + route[i - 1].y) / 2, (hit.point.z + route[i - 1].z) / 2);
                        lineRotation.eulerAngles = new Vector3(0f, - Mathf.Atan((hit.point.z - route[i - 1].z) / (hit.point.x - route[i - 1].x)) * Mathf.Rad2Deg, 0f);
                        lineScale = new Vector3(Mathf.Sqrt(Mathf.Pow((hit.point.z - route[i - 1].z), 2) + Mathf.Pow((hit.point.x - route[i - 1].x), 2)), line.transform.localScale.y, line.transform.localScale.z);

                        
                        line.transform.position = lineLocation;
                        line.transform.rotation = lineRotation;
                        line.transform.localScale = lineScale;
                        waypoints.Add(line);
                    }
                    i += 1;
                    placed = true;
                }
            }
        }
        else if(managerScript.mouseScript.manipulateOption != MouseScript.LevelManipulation.SelectRoute)
        {
            route = new List<Vector3>();
            i = 0;
            foreach(GameObject elt in waypoints)
            {
                Destroy(elt);
            }
            placed = false;
        }



    

}


    public void bind()
    {
        if (placed && managerScript.selectionManager.selectedUnits.Count > 0)
        {
            foreach (Transform j in managerScript.selectionManager.selectedUnits)
            {
                if (routeDict.ContainsKey(j.gameObject))
                {
                    routeDict.Remove(j.gameObject);
                    routeTimerDict.Remove(j.gameObject);
                }
                routeDict.Add(j.gameObject, route);
                routeTimerDict.Add(j.gameObject, float.Parse(timeTrigger.text));

                j.GetComponent<EditorObject>().data.route = true;
                j.GetComponent<EditorObject>().data.routeWaypoints = route;
                j.GetComponent<EditorObject>().data.triggerTimer = float.Parse(timeTrigger.text);


                if (!j.gameObject.TryGetComponent<NavMeshAgent>(out navMeshAgent))
                {
                    navMeshAgent = j.gameObject.AddComponent<NavMeshAgent>();
                    navMeshAgent.speed = 1.5f;
                    navMeshAgent.acceleration = 8;
                    navMeshAgent.angularSpeed = 10000;
                }
            }
        }
    }

    void routing()
    {
        if (routeDict.Count == 0) return;
        foreach (GameObject item in routeDict.Keys)
        {

            navMeshAgent = item.GetComponent<NavMeshAgent>();
            if (item.gameObject.TryGetComponent<Animator>(out animator))
            {
                if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
                {
                    animator.SetBool("isWalking", true);
                }
                else
                {
                    animator.SetBool("isWalking", false);
                }
            }


            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude <= 5) navMeshAgent.acceleration = 500;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 8) navMeshAgent.acceleration = 8;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 5) continue;

            routeDict.TryGetValue(item, out routeTaken);
            routeTimerDict.TryGetValue(item, out targetTime);

            targetTime -= Time.deltaTime;

            if (targetTime <= 0.0f)
            {
                navMeshAgent.isStopped = true;
                navMeshAgent.SetDestination(routeTaken[0]);
                navMeshAgent.isStopped = false;

                if ((navMeshAgent.destination - item.transform.position).sqrMagnitude <= 0.01)
                {
                    if (routeTaken.Count > 1)
                    {
                        routeTaken.RemoveAt(0);
                        routeDict.Remove(item);
                        routeDict.Add(item, routeTaken);
                        break;
                    }
                }
            }
            else
            {
                routeTimerDict.Remove(item);
                routeTimerDict.Add(item, targetTime);
            }


        }
    }
}
