﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialButton : MonoBehaviour
{
    private string Name;
        public Text ButtonText;
        public QuestingSystem ScrollView;

        public void SetName(string name)
        {
            Name = name;
            ButtonText.text = name;
        }
}
