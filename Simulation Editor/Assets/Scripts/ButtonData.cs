﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonData : MonoBehaviour
{
    public string createdAsset;
    public ManagerScript ms;
    public Sprite buttonIcon;

    private Image imageItem;
    private Button myButton;

    private void Awake()
    {
        myButton = GetComponent<Button>();
        imageItem = this.transform.GetChild(0).GetComponentInChildren<Image>();
    }

    private void Start()
    {
        myButton.onClick.AddListener(buttonFunction);
        imageItem.sprite = buttonIcon;
    }

    private void buttonFunction()
    {
        ms.ChooseCreatedItem(createdAsset);
    }
}
